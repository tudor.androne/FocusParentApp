package com.example.focusparentapp.Presentation.DeviceUse

import android.util.Log
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.SideEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import coil.compose.rememberImagePainter
import com.example.focusparentapp.R
import com.example.focusparentapp.RoomDB.ViewModels.AppStats
import com.example.focusparentapp.RoomDB.ViewModels.ScreenTracker
import com.example.focusparentapp.RoomDB.ViewModels.UsersViewModel
import com.example.focusparentapp.Utils.Utils
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext

@Composable
fun DeviceUse(navController: NavController, userId: String, usersViewModel: UsersViewModel) {

    val systemUiController = rememberSystemUiController()
    SideEffect {
        systemUiController.setSystemBarsColor(Color(0xFF172238))
    }
    val screenWidth = LocalConfiguration.current.screenWidthDp.dp
    val colorStops = arrayOf(
        0.2f to Color(0xFF172238),
        0.5f to Color(0xFF121B2E),
        1f to Color(0xFF0A101E)
    )

    var maxTimeSpent = 0

    var isLoading by remember { mutableStateOf(true) }
    var screenTracker by remember { mutableStateOf<ScreenTracker>(ScreenTracker(0, "")) }
    var appsStats by remember { mutableStateOf<List<AppStats>>(emptyList()) }
    var selectedTimeInterval by remember { mutableStateOf("1 Day") }
    var filteredStats by remember { mutableStateOf<List<AppStats>>(emptyList()) }

    LaunchedEffect(userId) {
        isLoading = true
        Log.d("DeviceUse", "LaunchedEffect started, isLoading set to true")
        try {
            while (true) {
                val newScreenTracker = withContext(Dispatchers.IO) {
                    Log.d("DeviceUse", "Fetching screenTracker")
                    usersViewModel.getScreenTimeTracker(userId)
                }
                val newAppsStats = withContext(Dispatchers.IO) {
                    Log.d("DeviceUse", "Fetching appsStats")
                    usersViewModel.getStatsFromUser(userId)
                }

                if (newScreenTracker != null && newAppsStats != null) {
                    screenTracker = newScreenTracker
                    appsStats = newAppsStats
                    filteredStats = filterAndSortStats(newAppsStats, selectedTimeInterval)
                    maxTimeSpent = filteredStats.maxOfOrNull { it.oneDayStats.toInt() } ?: 0
                    Log.d("DeviceUse", "Data fetched successfully")
                    break
                } else {
                    Log.d("DeviceUse", "Data is null, retrying...")
                    delay(500)
                }
            }
        } catch (e: Exception) {
            Log.e("DeviceUse", "Error fetching data", e)
            e.printStackTrace()
        } finally {
            isLoading = false
            Log.d("DeviceUse", "isLoading set to false")
        }
    }

    LaunchedEffect(selectedTimeInterval) {
        filteredStats = filterAndSortStats(appsStats, selectedTimeInterval)
        maxTimeSpent = filteredStats.maxOfOrNull { it.oneDayStats.toInt() } ?: 0
    }



    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(brush = Brush.linearGradient(colorStops = colorStops))
    ) {
        com.example.focusparentapp.Presentation.Apps.TopBar(
            navController = navController,
            route = "userMenu/${userId}",
            lastUpdateDate = ""
        )
        if (isLoading) {
            Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
                CircularProgressIndicator(color = Color.White)
            }
        } else {
            Spacer(modifier = Modifier.fillMaxHeight(.08f))
            Column(
                modifier = Modifier.padding(
                    start = screenWidth * 0.05f,
                    end = screenWidth * 0.05f,
                    top = screenWidth * 0.05f,
                    bottom = screenWidth * 0.05f
                )
            ) {

                Row() {
                    Card(
                        modifier = Modifier
                            .padding(end = 4.dp)
                            .border(
                                border = BorderStroke(1.dp, Color.White),
                                shape = RoundedCornerShape(15.dp)
                            )
                            .fillMaxWidth(.5f)
                            .fillMaxHeight(.2f),
                        colors = CardDefaults.cardColors(
                            containerColor = Color(0xFF22355C)
                        )
                    ) {

                        Text(
                            text = "Apps Launch Tracker ",
                            modifier = Modifier.padding(10.dp),
                            fontFamily = FontFamily(Font(R.font.opensans_res)),
                            color = Color.White
                        )
                        Row() {
                            Text(
                                text = screenTracker?.launchTracker.toString() ?: "Collecting data",
                                modifier = Modifier.padding(10.dp),
                                fontFamily = FontFamily(Font(R.font.opensans_res)),
                                fontSize = 30.sp,
                                color = Color.White
                            )
                            Text(
                                text = "launches /24h",
                                fontFamily = FontFamily(Font(R.font.opensans_res)),
                                modifier = Modifier.padding(start = 15.dp, top = 40.dp),
                                fontSize = 12.sp,
                                color = Color.White
                            )
                        }


                    }
                    Card(
                        modifier = Modifier
                            .padding(start = 4.dp)
                            .border(
                                border = BorderStroke(1.dp, Color.White),
                                shape = RoundedCornerShape(15.dp)
                            )
                            .fillMaxWidth(1f)
                            .fillMaxHeight(.2f),
                        colors = CardDefaults.cardColors(
                            containerColor = Color(0xFF121B2E)
                        )
                    ) {
                        Text(
                            text = "Screen Time",
                            modifier = Modifier.padding(10.dp),
                            fontFamily = FontFamily(Font(R.font.opensans_res)),
                            color = Color.White
                        )

                        Text(
                            text = screenTracker?.screenTime ?: "Collecting data",
                            modifier = Modifier.padding(10.dp),
                            fontFamily = FontFamily(Font(R.font.opensans_res)),
                            fontSize = 30.sp,
                            color = Color.White
                        )
                    }
                }

                Spacer(modifier = Modifier.fillMaxHeight(.08f))

                Row(modifier = Modifier.fillMaxWidth()) {
                    timeIntervalButton("1 Day", selectedTimeInterval == "1 Day") {
                        selectedTimeInterval = "1 Day"
                    }
                    timeIntervalButton("3 Days", selectedTimeInterval == "3 Days") {
                        selectedTimeInterval = "3 Days"
                    }
                    timeIntervalButton("1 Week", selectedTimeInterval == "1 Week") {
                        selectedTimeInterval = "1 Week"
                    }
                    timeIntervalButton("1 Month", selectedTimeInterval == "1 Month") {
                        selectedTimeInterval = "1 Month"
                    }
                }

                LazyColumn(
                    modifier = Modifier.padding(start = 10.dp)
                ) {
                    items(filteredStats.size) {
                        val it = filteredStats[it]
                        if (it.oneDayStats > maxTimeSpent)
                            maxTimeSpent = it.oneDayStats.toInt()

                        var progress = (it.oneDayStats.toFloat()) / (maxTimeSpent.toFloat())

                        Row(
                            verticalAlignment = Alignment.CenterVertically,
                            horizontalArrangement = Arrangement.spacedBy(5.dp),
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(8.dp)
                        ) {
                            Image(
                                painter = rememberImagePainter(
                                    data = Utils().byteStringToDrawable(
                                        it.icon
                                    )
                                ),
                                contentDescription = null,
                                modifier = Modifier.size(47.dp)
                            )
                            Text(
                                it.appName,
                                color = Color.White,
                                fontFamily = FontFamily(
                                    Font(R.font.opensans_res)
                                ),
                                modifier = Modifier.padding(10.dp),
                                fontWeight = FontWeight.Bold
                            )
                            Spacer(modifier = Modifier.weight(1f))
                            Text(
                                text = Utils.TimeUtils.convertMillisecondsToTime(
                                    it.oneDayStats
                                ),
                                color = Color.White,
                                fontFamily = FontFamily(
                                    Font(R.font.opensans_res)
                                ),
                            )

                        }

                        CustomLinearProgressIndicator(
                            progress = progress,
                            modifier = Modifier.fillMaxWidth(.95f)
                        )
                    }
                }
            }
        }
    }
}


@Composable
fun CustomLinearProgressIndicator(
    modifier: Modifier = Modifier,
    progress: Float,
    progressColor: Color = Color(0xFFF00D0D),
    backgroundColor: Color = Color(0xFF680000),
    clipShape: RoundedCornerShape = RoundedCornerShape(16.dp)
) {
    Box(
        modifier = modifier
            .clip(clipShape)
            .background(backgroundColor)
            .height(8.dp)
    ) {
        Box(
            modifier = Modifier
                .background(progressColor)
                .fillMaxHeight()
                .fillMaxWidth(progress)
        )
    }
}


@Composable
fun statsWidget(){
    Card(
        modifier = Modifier
            .padding(start = 4.dp)
            .border(
                border = BorderStroke(1.dp, Color.White),
                shape = RoundedCornerShape(15.dp)
            )
            .fillMaxWidth(.5f)
            .fillMaxHeight(.5f),
        colors = CardDefaults.cardColors(
            containerColor = Color(0xFF0E1B5F)
        )
    ) {
        Text(
            text = "Screen Time ",
            modifier = Modifier.padding(10.dp),
            fontFamily = FontFamily(Font(R.font.opensans_res)),
            color = Color.White
        )

        Text(
            text = "totalTimeInMillis",
            modifier = Modifier.padding(10.dp),
            fontFamily = FontFamily(Font(R.font.opensans_res)),
            fontSize = 30.sp,
            color = Color.White
        )
    }
}


@Composable
fun timeIntervalButton(
    timeInterval : String,
    isSelected : Boolean,
    onClick: () -> Unit
){

    val borderColor = if (isSelected) Color.Gray else Color.White
    val interactionSource = remember { MutableInteractionSource() }
    Card (
        modifier =
        Modifier
            .clickable(interactionSource = interactionSource, indication = null, onClick = onClick)
            .padding(bottom = 10.dp, start= 10.dp)
            .border(
                border = BorderStroke(1.dp, borderColor),
                shape = RoundedCornerShape(10.dp)
            )
    ){
        Text(
            text = timeInterval,
            color = borderColor,
            fontFamily = FontFamily(Font(R.font.opensans_res)),
            modifier = Modifier.padding(10.dp)
        )
    }
}

fun filterAndSortStats(stats: List<AppStats>, interval: String): List<AppStats> {
    val filteredStats = when (interval) {
        "1 Day" -> stats // Assuming oneDayStats is default
        "3 Days" -> stats.map { it.copy(oneDayStats = it.threeDaysStats) }
        "1 Week" -> stats.map { it.copy(oneDayStats = it.oneWeekStats) }
        "1 Month" -> stats.map { it.copy(oneDayStats = it.oneMonthStats) }
        else -> stats
    }
    return filteredStats.sortedByDescending { it.oneDayStats }
}

