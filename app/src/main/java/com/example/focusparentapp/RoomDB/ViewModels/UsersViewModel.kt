package com.example.focusparentapp.RoomDB.ViewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.example.focusparentapp.RoomDB.DAO.UsersDAO
import com.example.focusparentapp.RoomDB.Entities.BlockedWebsiteEntity
import com.example.focusparentapp.RoomDB.Entities.LocationCoordinatesEntity
import com.example.focusparentapp.RoomDB.Entities.PackageEntity
import com.example.focusparentapp.RoomDB.Entities.PackageStatsEntity
import com.example.focusparentapp.RoomDB.Entities.RestrictedKeywordsEntity
import com.example.focusparentapp.RoomDB.Entities.ScreenTimeTrackerEntity
import com.example.focusparentapp.RoomDB.Entities.UserEntity
import com.example.focusparentapp.RoomDB.Relations.UserPackageCrossRef
import com.example.focusparentapp.RoomDB.Relations.UserWithPackages
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch

class UsersViewModel (private val userDao: UsersDAO) : ViewModel() {

    suspend fun insertUserAndPackages(user: UserEntity, packages: List<PackageEntity>, timeSpent: List<Long>) {

        userDao.insertUser(user)

        packages.forEach { packageEntity ->
            userDao.insertPackage(packageEntity)
        }
        val userPackages = mutableListOf<UserPackageCrossRef>()

        packages.forEachIndexed { index, packageEntity ->
            val userPackageCrossRef = UserPackageCrossRef(user.userId, packageEntity.packageName, timeSpent[index])
            userPackages.add(userPackageCrossRef)
        }
        userDao.insertUserPackages(userPackages)
    }

    suspend fun insertUser(user: UserEntity) {
        userDao.insertUser(user)
    }



    fun getAllUsers(): Flow<List<UserEntity>> {
        return userDao.getAllUsers()
    }

    fun getUserById(userId: String): LiveData<UserEntity> {
        return userDao.getUserById(userId)
    }

    suspend fun updateUser(user: UserEntity) {
        userDao.updateUser(user)
    }

    suspend fun getAppsInfoFromUser(userId: String) : List<AppInfo>{
        return userDao.getAppsInfoFromUser(userId).map {
            AppInfo(it.appName, it.packageName, it.icon)
        }
    }
    suspend fun getAppsInfoAlphabeticallyOrdered(userId: String) : List<AppInfo>{
        return userDao.getAppsInfoAlphabeticallyOrdered(userId).map {
            AppInfo(it.appName, it.packageName, it.icon)
        }
    }


   suspend fun getUserWithPackages(userId : String) : LiveData<List<UserWithPackages>> {
       return userDao.getUserWithPackages(userId).asLiveData()
   }

    suspend fun getDeviceType(userId : String) : String {
        return userDao.getDeviceType(userId)
    }

    suspend fun getUserEmail(userId : String) : String {
        return userDao.getUserEmail(userId)
    }
    suspend fun updateIsBlocked(userId : String, packageName : String, isBlocked : Boolean){
        return userDao.updateIsBlocked(userId, packageName, isBlocked)
    }

    suspend fun getBlockedAppProperty(userId: String, packageName: String) : Boolean{
        return userDao.getBlockedAppProperty(userId,packageName)
    }

    suspend fun getAllBlockedApps (userId : String) : List<String> {
        return userDao.getAllBlockedApps(userId)
    }

    suspend fun removeBlockedWebsite(userId: String, websiteURL : String){
        userDao.removeBlockedWebsite(userId, websiteURL)
    }

    suspend fun insertBlockedWebsite(blockedWebsiteEntity: BlockedWebsiteEntity) = viewModelScope.launch {
        userDao.insertBlockedWebsite(blockedWebsiteEntity)
    }
    suspend fun getBlockedWebsites(userId: String) : List<String>{
        return userDao.getBlockedWebsites(userId)
    }

    suspend fun removeRestrictedKeyword(userId: String, restrictedKeyword : String){
        userDao.removeRestrictedKeyword(userId, restrictedKeyword)
    }

    suspend fun getRestrictedKeywords(userId: String) : List<String>{
        return userDao.getRestrictedKeywords(userId)
    }

    suspend fun insertRestrictedKeyword(restrictedKeywordsEntity: RestrictedKeywordsEntity) = viewModelScope.launch {
        userDao.insertRestrictedKeyword(restrictedKeywordsEntity)
    }


    fun getBlockedWebsitesAsFlow(userId: String): Flow<List<String>> {
        return userDao.getBlockedWebsitesAsFlow(userId)
    }

    fun getRestrictedKeywordsAsFlow(userId: String): Flow<List<String>> {
        return userDao.getRestrictedKeywordsAsFlow(userId)
    }

    fun getTimeSpentByUser(userId : String) : Flow<List<TimeSpentByUser>> {
        return userDao.getTimeSpentByUser(userId)
    }

    fun updateTimeSpent(userId: String, packageName: String, newTimeSpent: Long, lastTimeUpdated : String) {
        userDao.updateTimeSpent(userId, packageName, newTimeSpent, lastTimeUpdated)
    }

    fun getLastTimeUpdated(userId: String) : Flow<String> {
        return userDao.getLastTimeUpdated(userId)
    }

    suspend fun insertPackageStats(packageStatsEntity: PackageStatsEntity) {
        userDao.insertPackageStats(packageStatsEntity)
    }

    suspend fun updateTrackerAndTimeSpent(screenTimeTrackerEntity: ScreenTimeTrackerEntity) {
        userDao.updateTrackerAndTimeSpent(screenTimeTrackerEntity)
    }


    suspend fun getStatsFromUser(userId: String) : List<AppStats>{
        return userDao.getStatsFromUser(userId)
    }
    fun getScreenTimeTracker(userId: String) : ScreenTracker {
        return userDao.getScreenTimeTracker(userId)
    }

    //LOCATION
    suspend fun insertLocationCoordinates(locationCoordinatesEntity: LocationCoordinatesEntity) {
        userDao.insertLocationCoordinates(locationCoordinatesEntity)
    }

    fun getCoordinates(userId: String) : Flow<LocationCoordinates> {
        return userDao.getCoordinates(userId)
    }
}

data class AppInfo(
    val appName: String,
    val packageName: String,
    val icon : String
)

data class TimeSpentByUser(
    val packageName: String,
    val timeSpent: Long
)

data class AppStats(
    val appName: String,
    val icon : String,
    val oneDayStats : Long,
    val threeDaysStats : Long,
    val oneWeekStats : Long,
    val oneMonthStats : Long
)

data class ScreenTracker(
    val launchTracker: Int,
    val screenTime: String
)

data class LocationCoordinates(
    val longitude: Double,
    val latitude: Double,
    val timestamp: String
)
